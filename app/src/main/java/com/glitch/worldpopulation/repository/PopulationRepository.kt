/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.glitch.worldpopulation.repository


import android.arch.lifecycle.MutableLiveData
import com.glitch.worldpopulation.AppExecutors
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Repository that handles population api
 */

@Singleton
class PopulationRepository @Inject
internal constructor(appExecutors: AppExecutors) {
    private val API_URL = "http://api.population.io"

    val countries = MutableLiveData<List<String>>()

    init {
        countries.value = emptyList()

        val retrofit = Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        val api = retrofit.create(ApiService::class.java)
        val call = api.countries()

        appExecutors.onNetwork {
            countries.postValue(call.execute().body()?.countries)
        }
    }
}
