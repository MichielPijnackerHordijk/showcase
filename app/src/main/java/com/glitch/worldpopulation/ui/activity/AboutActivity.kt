package com.glitch.worldpopulation.ui.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.glitch.worldpopulation.R

class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
    }
}
