package com.glitch.worldpopulation.ui.viewholder

import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import com.glitch.worldpopulation.R
import com.glitch.worldpopulation.databinding.ItemCountryBinding

class CountryViewHolder(parent: ViewGroup, private val onCountryClick: (String) -> Unit) :
        DataBoundViewHolder<ItemCountryBinding>(DataBindingUtil.inflate(LayoutInflater.from(
                parent.context), R.layout.item_country, parent, false)) {
    var country: String? = null
        set(value) {
            field = value
            binding.countryName = value
        }

    init {
        itemView.setOnClickListener { country?.let { onCountryClick(it) } }
    }
}