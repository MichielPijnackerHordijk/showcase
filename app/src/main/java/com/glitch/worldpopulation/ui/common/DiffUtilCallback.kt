package com.glitch.worldpopulation.ui.common

/**
 * Created by glitch on 21-3-18.
 */
interface DiffUtilCallback<in T> {
    fun areItemsTheSame(oldItem: T, newItem: T): Boolean
    fun areContentsTheSame(oldItem: T, newItem: T): Boolean
    fun getChangePayload(oldItem: T, newItem: T): Any? = null
}