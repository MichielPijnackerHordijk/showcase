/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.glitch.worldpopulation.ui.common

import android.support.v4.app.Fragment
import com.glitch.worldpopulation.extensions.runIf
import com.glitch.worldpopulation.R
import com.glitch.worldpopulation.ui.activity.MainActivity
import com.glitch.worldpopulation.ui.fragment.CountryFragment
import com.glitch.worldpopulation.ui.fragment.ListFragment
import javax.inject.Inject

/**
 * A utility class that handles navigation in [MainActivity].
 */
class NavigationController @Inject constructor(mainActivity: MainActivity) {

    private val fragmentManager = mainActivity.supportFragmentManager


    fun navigateToList() {
        navigateTo("list", false) {
            ListFragment()
        }
    }

    fun navigateToCountry(country: String) {
        navigateTo("country/$country") {
            CountryFragment.create(country)
        }
    }

    private fun navigateTo(tag: String, addToBackStack: Boolean  = true, fragment: () -> Fragment) {
        if (fragmentManager.findFragmentByTag(tag)?.isVisible == true) {
            return
        }

        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment.invoke(), tag)
                .runIf(addToBackStack) { addToBackStack(null) }
                .commit()
    }
}